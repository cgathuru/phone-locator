package com.ctech.media.phonelocator;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

/**
 * This receiver will receive the battery status information and then
 * start a service to deal with the battery status appropriately.
 */
public class BatteryStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // TODO ask the user if they want to keep the service on
        SharedPreferences mPreferences =  context.getSharedPreferences(PhoneLocatorService.PHONE_LOC_SERVICE_PREF, Context.MODE_PRIVATE);
        boolean serviceDisabled = mPreferences.getBoolean(PhoneLocatorService.PREF_DISABLE_LOC_SERVICE, false);
        if (!serviceDisabled) {
            sendNotification(context);
            IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = context.registerReceiver(null, filter);
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;
            int percent = (level * 100) / scale;

            if (!isCharging && percent < 5) {
                Intent i = new Intent(context, PhoneLocatorService.class);
                context.startService(i);
            }
        } else {
            // Service disabled
            IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = context.registerReceiver(null, filter);
            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;
            // Enable phone location services if charging
            if (isCharging) {
                SharedPreferences.Editor prefEditor = mPreferences.edit();
                prefEditor.putBoolean(PhoneLocatorService.PREF_DISABLE_LOC_SERVICE, false);
                prefEditor.apply();
            }

        }

    }

    public void sendNotification(Context context) {
        final String content = "Your battery is low and is not charging. Would you like to" +
                "disable the Phone Locator Service? ONLY disable if you not in a position " +
                "where you might loose your phone. When connected to a charger the service will " +
                "re-enable.";
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.notification_template_icon_bg)
                .setContentTitle("Phone Locator Service")
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_MAX);

        Intent resultIntent =  new Intent(context, ResultActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // The stack builder object will contain an artificial back stack for the started
        // activity. This ensures that navigating backward from the Activity leads out of
        // the application to the Home Screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not for the Intent itself)
        stackBuilder.addParentStack(ResultActivity.class);
        // Adds the Intent that start the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(1, mBuilder.build());
    }
}
