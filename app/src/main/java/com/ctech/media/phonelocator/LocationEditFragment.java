package com.ctech.media.phonelocator;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * This fragment shows the view that allows the user to modify the location message that
 * will be sent to the users selected contact
 */
public class LocationEditFragment extends Fragment {

    EditText mCurrentMessage;
    Button mApplyButton;

    public static final String LOGTAG = "LocationEditFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        mCurrentMessage = (EditText) view.findViewById(R.id.fragment_location_text_edit);
        mApplyButton = (Button) view.findViewById(R.id.fragment_location_apply);
        mApplyButton.setOnClickListener(applyListener);
        return view;
    }

    Button.OnClickListener applyListener = new Button.OnClickListener(){

        @Override
        public void onClick(View view) {
            SharedPreferences preferences = getActivity().getSharedPreferences(PhoneLocatorService.PHONE_LOC_SERVICE_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor prefEditor = preferences.edit();
            final String locationMsg = mCurrentMessage.getText().toString();
            prefEditor.putString(PhoneLocatorService.PREF_MSG_LOC_KEY, locationMsg);
            prefEditor.apply();
            getActivity().onBackPressed();
        }
    };
}
