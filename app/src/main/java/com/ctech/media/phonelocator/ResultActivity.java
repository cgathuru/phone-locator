package com.ctech.media.phonelocator;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by EGATCHA on 8/12/2015.
 */
public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences mPreferences = getSharedPreferences(PhoneLocatorService.PHONE_LOC_SERVICE_PREF, MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = mPreferences.edit();
        prefEditor.putBoolean(PhoneLocatorService.PREF_DISABLE_LOC_SERVICE, true);
        prefEditor.commit();
        finish();
    }
}
