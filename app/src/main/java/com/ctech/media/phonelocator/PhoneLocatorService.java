package com.ctech.media.phonelocator;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.util.Log;

import java.io.IOException;
import java.util.List;


/**
 * This services locates the phone and sends that location
 * to a given number, specified in the preferences.
 */
public class PhoneLocatorService extends Service {

    private LocationManager mLocManager;
    private Location mBestLocation;
    private int mStartId;
    private SharedPreferences mPreferences;

    private static final int TWO_MINUTES = 100 * 60 * 2;
    private static final int FIVE_MINUTES = 100 * 60 * 5;

    // Delayed stop handler messages
    private static final int STOP_LOC_SERVICES = 0;

    // Service location handler messages
    private static final int SEND_LOCATION = 0;
    private static final int STOP_SERVICE = 1;

    public static final String PREF_DISABLE_LOC_SERVICE = "PhoneLocator";
    public static final String PHONE_LOC_SERVICE_PREF = "PhoneLocatorService";
    public static final String PREF_CONTACT_KEY = "PhoneLocatorServiceContact";
    public static final String PREF_NUMBER_KEY = "PhoneLocatorServiceNumber";
    public static final String PREF_MSG_KEY = "PhoneLocatorServiceMessage";
    public static final String PREF_MSG_LOC_KEY = "PhoneLocatorServiceMsgLoc";

    private static String LOGTAG = "PhoneLocatorService";

    private final Handler mDelayedStopHandler =  new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what){
                case STOP_LOC_SERVICES:
                    mLocManager.removeUpdates(locationListener);
                    //mLocServiceHandler.sendEmptyMessage(SEND_LOCATION);
                    break;
                default:
                    Log.d(LOGTAG, "Unknown action " + msg.what);
                    break;
            }
        }
    };

    private final Handler mLocServiceHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SEND_LOCATION:
                    mDelayedStopHandler.sendEmptyMessage(STOP_LOC_SERVICES);
                    sendLocationToContact();
                    stopSelf(mStartId);
                    break;
                case STOP_SERVICE:
                    stopSelf(mStartId);
                    break;
                default:
                    Log.d(LOGTAG, "Unknown action " + msg.what);
                    break;
            }
        }
    };

    public void sendLocationToContact(){
        final String phoneNumber = getPreferredContactNumber();
        final String phoneMessage = getPhoneMessage();
        final String locMessage = appendLocToLocMsg(getLocationMessage());
        final String message = appendLocTocMsg(phoneMessage, locMessage);
        sendSMS(phoneNumber, message);
    }

    private String appendLocTocMsg(String msg, String locMsg) {
        return "" + msg + "\n" + locMsg;
    }

    public String appendLocToLocMsg(String msg) {
        return appendLocToLocMsg(msg, mBestLocation);
    }

    public String appendLocToLocMsg(String msg, Location location) {
        Geocoder gCoder = new Geocoder(this);
        List<Address> addresses = null;
        try {
            addresses = gCoder.getFromLocation(location.getLatitude(), location.getLongitude(),1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if ( addresses != null && addresses.size() > 0 ) {
            return "" + msg + " " + "country: " + addresses.get(0).getCountryName() +
                    ", city: " + addresses.get(0).getLocality() +
                    ", state: " + addresses.get(0).getAdminArea() +
                    ", address: " + addresses.get(0).getAddressLine(0) +
                    ", known name: " + addresses.get(0).getFeatureName() +
                    ", postal code: " + addresses.get(0).getPostalCode();
        }
        return  "" + msg + " " + location.getLatitude() + ", " + location.getLongitude();
    }

    private String getLocationMessage() {
        return mPreferences.getString(PREF_MSG_LOC_KEY, "");
    }

    private String getPhoneMessage() {
        return mPreferences.getString(PREF_MSG_KEY, "");
    }

    private void sendSMS(final String phoneNumber, final String message) {
        final String SENT = "SMS_SENT";
        final String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this,0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

        registerReceiver(smsReceiver, new IntentFilter(SENT));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    BroadcastReceiver smsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    mLocServiceHandler.sendEmptyMessage(STOP_SERVICE);
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    break;
            }
        }
    };

    BroadcastReceiver batteryChargeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
            int percent = (level*100)/scale;
            boolean isCharging =  status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;
            if (isCharging) {
                mDelayedStopHandler.removeCallbacksAndMessages(null);
                mDelayedStopHandler.sendEmptyMessage(STOP_LOC_SERVICES);
                mLocServiceHandler.sendEmptyMessage(STOP_SERVICE);
            }

            if (mLocManager!=null && !isCharging && percent < 2) {
                // Battery going to die any moment
                mDelayedStopHandler.removeCallbacksAndMessages(null);
                mLocServiceHandler.sendEmptyMessage(SEND_LOCATION);
            }
        }
    };

    private String getPreferredContactNumber(){
        return mPreferences.getString(PREF_NUMBER_KEY, "");
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mBestLocation = null;
        mPreferences =  getSharedPreferences(PHONE_LOC_SERVICE_PREF, MODE_PRIVATE);
        if (mLocManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
            mLocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        mDelayedStopHandler.sendEmptyMessageDelayed(STOP_LOC_SERVICES, FIVE_MINUTES);
        mStartId = startId;
        registerReceiver(batteryChargeReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        try {
            unregisterReceiver(smsReceiver);
        } catch (IllegalArgumentException ex) {
            Log.d(LOGTAG, "Receiver not registered " + ex);
        }
        unregisterReceiver(batteryChargeReceiver);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            makeUseOfNewLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public void makeUseOfNewLocation(Location currentLocation){
        if (isBetterLocation(currentLocation, mBestLocation))
            mBestLocation = currentLocation;
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if ( currentBestLocation == null)
            return true;

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since th current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
        // If the new location is more than two minutes older, it must be worse
        } else if(isSignificantlyOlder) {
            return false;
        }

        // Check if the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        return (provider1 == null) ? provider2 == null : provider1.equals(provider2);
    }
}
